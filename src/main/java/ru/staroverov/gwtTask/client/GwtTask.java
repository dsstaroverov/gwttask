package ru.staroverov.gwtTask.client;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.TextColumn;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.*;
import ru.staroverov.gwtTask.shared.Book;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class GwtTask implements EntryPoint {


    private String order="";
    private final MultiSelectionModel<Book> selectionModel = new MultiSelectionModel<Book>(Book::getId);
    private final GwtTaskServiceAsync service = GwtTaskService.App.getInstance();

    private MyDataProvider dataProvider = new MyDataProvider();


    private CellTable<Book> table = getTable();

    private AsyncCallback<List<Book>> updateCallback = new AsyncCallback<List<Book>>() {
        @Override
        public void onFailure(Throwable caught) {
            Window.alert(caught.getMessage());
        }
        @Override
        public void onSuccess(List<Book> result) {
            updateTable(result);
        }
    };

    private class MyDataProvider extends AsyncDataProvider<Book>{
        @Override
        protected void onRangeChanged(HasData<Book> display) {

            if(table.getColumnSortList().get(0).isAscending())
                {order = " b order by b.title";
                }
            else {
                order=" b order by b.title DESC";
            }

            service.getAll(order,updateCallback);

        }
    }


    public void onModuleLoad() {
        dataProvider.addDataDisplay(table);
        TextArea searchArea = new TextArea();
        searchArea.setCharacterWidth(25);

        final Button addBook = new Button("Add new book", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                getDialog(new Book());
            }
        });

        final Button refresh = new Button("Refresh", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                searchArea.setValue("");
                service.getAll(order,new AsyncCallback<List<Book>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert("Fail getAllBooks");
                    }

                    @Override
                    public void onSuccess(List<Book> result) {
                        updateTable(result);
                    }

                });


            }
        });

        final Button deleteSelected = new Button("Delete selected", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                Set<Integer> selectedId = new HashSet<>();

                for(Book book:selectionModel.getSelectedSet()) {
                    selectedId.add(book.getId());
                }
                service.deleteSelected(selectedId, new AsyncCallback<Boolean>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert("error delete selected book");
                    }

                    @Override
                    public void onSuccess(Boolean result) {
                        //Window.alert("okay delete");
                        service.getAll(order, updateCallback);

                    }
                });

            }
        });



        final Button searchParam = new Button("search", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                service.search(searchArea.getValue(), updateCallback);

            }
        });


        RootPanel.get("slot1").add(addBook);
        RootPanel.get("slot1").add(refresh);
        RootPanel.get("slot1").add(deleteSelected);
        RootPanel.get("slot1").add(searchArea);
        RootPanel.get("slot1").add(searchParam);
        RootPanel.get("slot2").add(table);
    }

    private void updateTable(List<Book> result){
        //table.setVisibleRange(0,result.size());
        //table.setRowCount(result.size());
        //table.setRowData(result);
        dataProvider.updateRowCount(result.size(),true);
        dataProvider.updateRowData(0,result);
    }

    private DialogBox getDialog(Book book){

        final DialogBox dialogBox = new DialogBox(false,true);
        dialogBox.setText("Add new book or edit");


        VerticalPanel vp = new VerticalPanel();
        //vp.setStyleName("background");

        Label titleLabel = new Label("Title");
        //titleLabel.setStyleName("gwt-Label");

        Label authorLabel = new Label("Author");
        //authorLabel.setStyleName("gwt-Label");

        Label descriptionLabel = new Label("Description");
        //Label.setStyleName("background");

        Label dateLabel = new Label("Date");
        //titleLabel.setStyleName("background");

        TextArea titleArea = new TextArea();
        titleArea.setCharacterWidth(20);
        TextArea authorArea = new TextArea();
        authorArea.setCharacterWidth(20);
        TextArea descriptionArea = new TextArea();
        descriptionArea.setCharacterWidth(20);
        DateBox dateArea = new DateBox();


        titleArea.setValue(book.getTitle());
        authorArea.setValue(book.getAuthor());
        descriptionArea.setValue(book.getDescription());
        dateArea.setValue(book.getReleaseTime());


        final Button apply = new Button("add/change", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                book.setTitle(titleArea.getValue());
                book.setAuthor(authorArea.getValue());
                book.setDescription(descriptionArea.getValue());
                book.setReleaseTime(dateArea.getValue());
                service.save(book,
                        new AsyncCallback<Book>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                Window.alert("Fail save");
                            }
                            @Override
                            public void onSuccess(Book result) {
                                service.getAll(order,updateCallback);
                            }
                        });

                dialogBox.hide();
            }
        });

        final Button cancelButton = new Button("cancel", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                dialogBox.hide();
            }
        });


        vp.setBorderWidth(3);

        vp.add(titleLabel);
        vp.add(titleArea);

        vp.add(authorLabel);
        vp.add(authorArea);

        vp.add(descriptionLabel);
        vp.add(descriptionArea);

        vp.add(dateLabel);
        vp.add(dateArea);

        vp.add(apply);
        vp.add(cancelButton);

        dialogBox.setWidget(vp);

        dialogBox.center();
        dialogBox.show();

        return dialogBox;
    }



    private CellTable<Book> getTable(){
        CellTable<Book> table = new CellTable<>();

        /*TextColumn<Book> idColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return String.valueOf(book.getId());
            }
        };*/

        table.setSelectionModel(selectionModel, DefaultSelectionEventManager.<Book> createCheckboxManager());



        Column<Book, Boolean> checkColumn = new Column<Book, Boolean>(
                new CheckboxCell(true, false)) {
            @Override
            public Boolean getValue(Book object) {
                // Get the value from the selection model.
                return selectionModel.isSelected(object);
            }
        };



        TextColumn<Book> titleColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return book.getTitle();
            }
        };
        titleColumn.setSortable(true);


        TextColumn<Book> authorColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return book.getAuthor();
            }
        };
        //authorColumn.setSortable(true);

        TextColumn<Book> descriptionColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                return book.getDescription();
            }
        };
        //descriptionColumn.setSortable(true);


        TextColumn<Book> releaseTimeColumn = new TextColumn<Book>() {
            @Override
            public String getValue(Book book) {
                DateTimeFormat format = DateTimeFormat.getFormat("dd.MM.yyyy");
                return format.format(book.getReleaseTime());
            }
        };
       // releaseTimeColumn.setSortable(true);


        ActionCell<Book> editCellAction = new ActionCell<Book>("Edit", new ActionCell.Delegate<Book>() {
            @Override
            public void execute(Book object) {
                getDialog(object);
                }

        });

        Column editColumn = new Column<Book,Book>(editCellAction) {
            @Override
            public Book getValue(Book object) {
                return object;
            }
        };

        ActionCell<Book> removeCellAction = new ActionCell<Book>("Remove", new ActionCell.Delegate<Book>() {
            @Override
            public void execute(Book object) {
                service.delete(object.getId(), new AsyncCallback<Boolean>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert("error delete");
                    }

                    @Override
                    public void onSuccess(Boolean result) {
                        service.getAll(order,updateCallback);

                    }
                });
            }
        });


        Column removeColumn = new Column<Book,Book>(removeCellAction) {
            @Override
            public Book getValue(Book object) {
                return object;
            }
        };



        //table.addColumn(idColumn,"id");
        table.addColumn(checkColumn);
        table.addColumn(titleColumn,"Title");
        table.addColumn(authorColumn,"Author");
        table.addColumn(descriptionColumn, "Description");
        table.addColumn(releaseTimeColumn,"Release Time");
        table.addColumn(editColumn,"Edit");
        table.addColumn(removeColumn,"Remove");




        ColumnSortEvent.AsyncHandler columnSortHandler = new ColumnSortEvent.AsyncHandler(table);
        table.addColumnSortHandler(columnSortHandler);


        table.getColumnSortList().push(table.getColumn(0));


        return table;
    }
}
