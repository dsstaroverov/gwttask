package ru.staroverov.gwtTask.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.staroverov.gwtTask.shared.Book;

import java.util.List;
import java.util.Set;


public interface GwtTaskServiceAsync {
    void getAll(String order,AsyncCallback<List<Book>> async);
    void save(Book book, AsyncCallback<Book> async);
    void delete(int id, AsyncCallback<Boolean> async);
    void deleteSelected(Set<Integer> set, AsyncCallback<Boolean> async);
    void search(String param, AsyncCallback<List<Book>> async);
}
