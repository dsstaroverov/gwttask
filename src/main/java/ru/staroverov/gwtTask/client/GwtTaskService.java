package ru.staroverov.gwtTask.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.staroverov.gwtTask.shared.Book;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Set;

@RemoteServiceRelativePath("gwtTaskService")
public interface GwtTaskService extends RemoteService {
    // Sample interface method of remote interface
    List<Book> getAll(String order);
    Book save(Book book);
    boolean delete(int id);
    boolean deleteSelected(Set<Integer> set);
    List<Book> search(String param);
    /**
     * Utility/Convenience class.
     * Use GwtTaskService.App.getInstance() to access static instance of GwtTaskServiceAsync
     */
    public static class App {
        private static GwtTaskServiceAsync ourInstance = GWT.create(GwtTaskService.class);

        public static synchronized GwtTaskServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
