package ru.staroverov.gwtTask.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.staroverov.gwtTask.client.GwtTaskService;
import ru.staroverov.gwtTask.shared.Book;


import java.util.List;
import java.util.Set;


public class GwtTaskServiceImpl extends RemoteServiceServlet implements GwtTaskService {

    private GwtTaskRepository repository = new GwtTaskRepository();

    public GwtTaskServiceImpl() {

    }


    @Override
    public List<Book> getAll(String order) {
        return repository.getAll(order);
    }

    @Override
    public Book save(Book book) {
        return repository.save(book);
    }

    @Override
    public boolean delete(int id) {
        return repository.delete(id);
    }

    @Override
    public boolean deleteSelected(Set<Integer> set) {
        return repository.deleteSelected(set);
    }

    @Override
    public List<Book> search(String param) {
        return repository.search(param);
    }
}