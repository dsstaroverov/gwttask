package ru.staroverov.gwtTask.server;

import ru.staroverov.gwtTask.shared.Book;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GwtTaskRepository {



    @PersistenceContext
    EntityManager em;

    public GwtTaskRepository() {
        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("jpaPersistenceUnit");
        em = managerFactory.createEntityManager();
    }

    public List<Book> getAll(String order) {
        Query query =em.createQuery("from Book"+order);
        return query.getResultList();
    }

    @Transactional
    public Book save(Book book){
        em.getTransaction().begin();
        if(book.getId()==null){
            em.persist(book);
        }
        else {
            em.merge(book);
        }
        em.getTransaction().commit();
        return book;
    }

    @Transactional
    public boolean delete(int id){
        Book book = em.find(Book.class,id);
        if(book!=null) {
            em.getTransaction().begin();
            em.remove(book);
            em.getTransaction().commit();
            return true;
        }
        return false;
    }

    @Transactional
    public boolean deleteSelected(Set<Integer> set){
        if(!set.isEmpty()){
            Set<Book> findBooks = new HashSet<>();
            for(Integer id:set){
                Book book = em.find(Book.class,id);
                if(book!=null){
                    findBooks.add(book);
                }
            }
            if(!findBooks.isEmpty()) {
                em.getTransaction().begin();
                for(Book book:findBooks){
                    em.remove(book);
                }
                em.getTransaction().commit();
                return true;
            }

        }
        return false;
    }

    public List<Book> search(String param) {
            Query query = em.createQuery("from Book b where b.title=:param or b.author=:param or b.description=:param order by b.title");
            return query.setParameter("param", param).getResultList();

    }
}
