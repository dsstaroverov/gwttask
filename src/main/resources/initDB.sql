DROP TABLE IF EXISTS books;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START WITH 1;


CREATE TABLE books (
  id          INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  title       VARCHAR      NOT NULL,
  author       VARCHAR      NOT NULL,
  description   VARCHAR      NOT NULL,
  release_time   TIMESTAMP DEFAULT now()NOT NULL
);
